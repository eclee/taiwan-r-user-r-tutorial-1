% 關於R Package的二三事
% Taiwan R User Group
% 2013-04-29




# Wush Wu

## R 經歷

- Taiwan R User Group 主辦人
- 自2008年開始學R 至今，R 是我第一個接觸的程式語言
    - Rcpp, inline, rJava
    - shiny
    - DBI, RSQLite, rredis

## R 著作

- Author of [*RMessenger*](http://cran.r-project.org/web/packages/RMessenger/index.html)

# 大綱

- R Package 簡介
- 使用R Package
    - 搜尋
    - 安裝
    - 閱讀說明
    - 使用
- 建立R Package(Demo)
    - R Package 結構簡介
    - 使用`Rstudio`建立R Package
    - 使用`Rstudio`安裝R Package
    - 使用`roxygen2` 建立文件
    - 使用`R CMD check`編寫測試
    - 使用`R CMD build`發佈R Package

# 簡介

<table>
<tr>
<td>
## 為什麼要使用R Package

目前有超過4000個R Packages，大大的擴充了R 的功能

## 使用R Package的注意事項

品質參差不齊，不保證正確

## 為什麼要建立R Package

[*Reproducibility*](http://en.wikipedia.org/wiki/Reproducibility)
</td>
<td>
<img src="img/ItWorksOnMyMachine.jpg" />
</td>
</tr>
</table>

# 使用R Package

讓我們簡單的回顧，如何使用別人寫好的Package吧！
<table><tr><td>

## 搜尋

想要尋找發e-mail相關的R套件嘛？
</td><td>
<img src="img/e-mail-google.png" />
</td></tr></table>

# 搜尋R Package -- Google and R Search

<table><tr><td>
R search
</td><td>
<img src="img/e-mail-R-search.png" />
</td></tr></table>

# 搜尋R Package -- R 套件集散地

<table><tr><td>
- [CRAN](http://cran.r-project.org/): 超過4000個套件
- [Bioconductor](http://www.bioconductor.org/): 約600個套件，主要和生物相關
- [Omegahat](http://www.omegahat.org/): 主要是R 和其他語言的套件
</td><td>
![](http://students.cis.uab.edu/rainer/Marketplace.jpg)
</td></tr></table>

# 安裝

<table><tr><td>

```r
install.packages("rredis")
```

</td><td>
![](img/Install.png)
</td></tr></table>

出錯了？請看看log

# 閱讀說明

<table><tr><td>

```r
library(rredis)
# library(help=rredis)
example(redisConnect)
```

```
## 
## rdsCnn> ## Not run: 
## rdsCnn> ##D redisConnect()
## rdsCnn> ##D redisSet('x',runif(5))
## rdsCnn> ##D redisGet('x')
## rdsCnn> ##D redisClose()
## rdsCnn> ## End(Not run)
## rdsCnn> 
## rdsCnn> 
## rdsCnn>
```


## 使用


```r
library(rredis)
```

</td><td>
![](img/Help.png)
</td><td>
![](img/Vignettes.png)
</td></tr></table>

# 建立R Package

# R Package 結構簡介

- DESCRIPTION: 重要檔案！描述Package的重要屬性
- NAMESPACE: 不需要去修改，我們利用roxygen2來更改
- man: 需要的時候清除，我們利用roxygen2來產生裏面的檔案
- R: R scripts，我們主要開發的地方
- data: 放置資料的地方
- src: 使用其他語言(如C、C++)開發擴充功能的地方
- tests: 建立套件測試的地方
- inst: 放置其他檔案的地方，安裝後會放到套件目錄底下
(`system.file("Files in inst", package="Package Name")`)

# 使用`Rstudio`建立R Package

<table><tr><td>
![](img/create-package.png)
</td><td>
![](img/init-structure.png)
</td></tr></table>

# 使用`Rstudio`安裝R Package

<table><tr><td>
將功能放置於`R`目錄之內


```r
rdemo_hello_world <- function() {
    print("hello world")
}
```


Build --> Configure Build Tools...

Build --> Build All


```r
library(RDemo)
rdemo_hello_world()  # 找不到吧
Rdemo:::rdemo_hello_world()  # 隱藏在namespace:Rdemo
```

</td><td>
![](img/build-tools.png)
</td></tr></table>

# 使用`roxygen2` 建立文件: export function

- 調整Build Tools...
- 撰寫function的註解
- Build!
- 注意: NAMESPACE檔案已經被修改


```r
#'@export
rdemo_hello_world <- function() {
    print("hello world")
}
```


# 使用`roxygen2` 建立文件: man

<table><tr><td>

```r
#'@title This is title
#'
#'@description This is description
#'
#'@details This is detials
#'
#'@param msg greeting message
#'@examples
#'rdemo_hello_world('examples')
#'
#'@seealso \link{package.skeleton}
#'@export
rdemo_hello_world <- function(msg) {
    print("hello world")
    print(msg)
}
```

</td><td>
![](img/roxygen2-result.png)
</td></tr></table>

# 使用`R CMD check`編寫測試

<table><tr><td>
## 測試script


```r
library(Rdemo)
rdemo_hello_world("greeting!")
```


放置於 `tests` 底下
</td><td>
Build --> Check Packages

![](img/cmd-check.png)
</td></tr></table>

# 使用`R CMD build`發佈R Package

<table><tr><td>
Build --> Build Source Package

發佈前最好先對`RDemo_1.0.tar.gz`做檢查:

```
R CMD check RDemo_1.0.tar.gz
R CMD check --as-cran RDemo_1.0.tar.gz 
```

Windows Binary

<http://win-builder.r-project.org/>

</td><td>
![](img/build-src-pkg.png)
</td></tr></table>
![](img/win-builder.png)

# Q&A

# 參考資料、引用資料

## 圖片來源

- <http://4.bp.blogspot.com/_CywCAU4HORs/S-HYtX4vOkI/AAAAAAAAAHw/Hzi5PYZOkrg/s1600/ItWorksOnMyMachine.jpg>
- <http://students.cis.uab.edu/rainer/Marketplace.jpg>
