# 2013年7月份 Taiwan R User Group 講題分享: 如何讓R與Java溝通 - rJava

R 是目前最熱門的 Open Source 統計語言。除了傳統的統計檢定之外，R也有套件支援許多 Machine Learning 和 Data Mining 的技術。因此使用者可以很方便的在R上實作各種分析方法。

[rJava](http://www.rforge.net/rJava/ "rJava官網") 在 [Top 100 R packages for 2013](http://www.r-statistics.com/2013/06/top-100-r-packages-for-2013-jan-may/ "Top 100 R packages for 2013 (Jan-May)!") 名單中排名第17名，是讓 R 呼叫 Java 撰寫的物件(Class)、實例(Instance)和方法(Method)的套件。這個套件降低R調用Java既有的資源的難度，例如[Google APIs](https://developers.google.com/apis-explorer/ "Google APIs")、[Hadoop](http://hadoop.apache.org/)等等。

# 安裝步驟

輸入`install.packages('rJava')`，選取CRAN位置之後，便完成安裝。

![install](install_s.png)

# 初始化`rJava`

每次在使用`rJava`之前，都要先啟動JVM：

```{r init}
library(rJava)
.jinit()
```

# Hello World

我們先依照[rJava官網文件](http://www.rforge.net/rJava/ "rJava官網")的介紹，來Demo一個Hello World吧！首先，使用[`.jnew`](http://www.rforge.net/doc/packages/rJava/jnew.html "jnew Doc")來產生一個[java.lang.String](http://docs.oracle.com/javase/6/docs/api/java/lang/String.html "String JavaDoc")的instance "s"，

```{r hello-world}
s <- .jnew("java/lang/String", "Hello World!")
s
```

從輸入s之後就可看出，s屬於Java-Object，且其內容為`Hello World!`。

# 建立Java物件的Reference

透過 `J`，使用者可以取出Java物件的Reference，進而存入一個R的變數：

```{r J}
pi <- J("java.lang.Math")
pi
```

# 建立Java實例

透過 `.jnew`，使用者可以建立某個Java物件的實例：

```{r jnew}
s <- .jnew("java/lang/String", "Hello World!")
s
```


# 取得Java物件的屬性值

取得Java物件的屬性有兩種方法：

- [`.jfield`](http://www.rforge.net/doc/packages/rJava/jfield.html "jfield Doc")
- `$`+Field名

```{r field}
.jfield("java.lang.Math",, "PI")
pi$PI
```  

# 使用物件的方法

呼叫Java物件的方法可以用

- [`.jcall`](http://www.rforge.net/doc/packages/rJava/jcall.html "jcall Doc")
    `.jcall`的第一個參數是java物件變數，第二個是Return Type(定義請見下圖，[出處](http://cran.r-project.org/web/packages/helloJavaWorld/vignettes/helloJavaWorld.pdf "Hello Java World! A Tutorial for Interfacing to
Java Archives inside R Packages."))，第三是Java物件的方法名稱。
- `$`+Method名

```{r method}
.jcall(s,"I","length")
s$length()
```  

若是要查詢物件的Method，則可以利用：

- `.jmethods`
- `names(obj)`
- `obj$`+`TAB`鍵(自動完成)

```{r list-method}
.jmethods(pi)
names(pi)
## pi$+TAB
```  

![obj$+TAB](methodTAB.png)

# 使用Java Library Jar

如果使用者要呼叫非JDK內建的物件(如：[SWT](http://www.eclipse.org/swt/ "SWT: The Standard Widget Toolkit"))，則必須先匯入定義該物件的jar檔。具體操作如下：

- 用`.jaddClassPath`設定R 搜尋`.jar`的路徑。

```{r jaddClassPath}
.jaddClassPath(dir("C:/rJava", full.names=TRUE))
```  

完成後，可用`.jclassPath()`來確認。

```{r jclassPath}
.jclassPath()
```

接下來，新增一個SWT的Display物件。

```{r display, eval=FALSE}
display <- .jnew("org/eclipse/swt/widgets/Display")
```  

再新增一個Shell物件來裝Display物件。

```{r shell, eval=FALSE}
shell <- .jnew("org/eclipse/swt/widgets/Shell", display)
shell$open()
```

便可產生出SWT視窗。

![SWT Demo](SWT_DEMO_s.png)

# Java 程式碼包裝成 R Package

接下來依照那，[Hello Java World! A Tutorial for Interfacing to Java Archives inside R Packages.](http://cran.r-project.org/web/packages/helloJavaWorld/vignettes/helloJavaWorld.pdf "Hello Java World! A Tutorial for Interfacing to
Java Archives inside R Packages.")，撰寫一個使用rJava的R Package。這樣所有之前繁瑣的設定動作，全部簡化成`library(xxx)`，而且需要的`jar`檔也可以隨著套件散佈，並安裝到適當的位置。

# 利用 Google SMTP 傳送 Email

首先，撰寫好Java傳送Email的程式打包成jar檔，放置套件根目錄(範例中是`C:\helloJavaWorld`)內的`inst\java`目錄下，並於套件根目錄的R目錄下，新增email.R：

```{r email.R, eval=FALSE}
email <- function(s,o){
  email <- .jnew("addEvent")
  email$GamilSender(s,o)
}
```

接下來要設定套件，讓email函數可以供其他使用者呼叫：

- 到套件根目錄下的`DESCRIPTION`檔案之內，在`Collate:`下加上`'email.R'`。
- 修改套件根目錄下的`NAMESPACE`檔案，在檔案中加上`export("email")`。

完成後，透過

```{r install, eval=FALSE}
install.packages("C:/helloJavaWorld", repos = NULL, type="source", INSTALL_opts="--no-multiarch")
```

安裝剛剛建立的套件後，就可以用`email`函數來寄信了！

![Email Demo](email_demo.png)

# SWT GUI While Loop 問題

以下是另一個使用SWT的範例，[rJavaTest.java](https://github.com/philipz/R_Capital_API "範例 GitHub 位置")。

因R 是單緒程(Single Thread)，所以直接使用SWT語法的while loop，會發生R一直停在那Java程式(blocking)。為了避免blocking，我們將SWT的使用方法改寫成使用Java Thread物件，並利用[Design Pattern](http://en.wikipedia.org/wiki/Design_pattern "Wikipedia Design Pattern") – [Singleton](http://en.wikipedia.org/wiki/Singleton_pattern "Wikipedia Singleton")來取值，這是用rJava呼叫Java程式時可能會遇到的狀況。

```{r blocking, eval=FALSE}
while (!shell.isDisposed()) {
  if (!display.readAndDispatch())
		display.sleep();
}
display.dispose();
```

![Singleton Demo](api_demo.png)

***

# 作者

## Philipz ([philipzheng@gmail.com](mailto:philipzheng@gmail.com))

- [Taiwan R User Group](https://www.facebook.com/Tw.R.User) Officer
- 研究領域：Image Processing, Software Engineering, Algorithmic Trading
- 開放原始碼專案：[TradingBot 程式交易機器人](https://github.com/philipz/FuturesBot)
- Blog: [Philipz學習日誌](http://server.everfine.com.tw/blog/)

## Wush Wu ([wush978@gmail.com](mailto:wush978@gmail.com))

- [Taiwan R User Group](https://www.facebook.com/Tw.R.User) Organizer
- R 相關著作：
    - [RMessenger](http://cran.r-project.org/web/packages/RMessenger/index.html)的作者
    - [RSUS](https://bitbucket.org/wush_iis/rsus)，這是[On Shortest Unique Substring Query](http://www.cs.sfu.ca/~jpei/publications/MISQ_ICDE12.pdf)的實作
- 研究領域：Large Scale Learning，[Text Mining](http://www.cs.sfu.ca/~jpei/publications/MISQ_ICDE12.pdf)和[Uncertain Time Series](http://www.cs.sfu.ca/~jpei/publications/Shortest%20Unique%20Substring%20Queries%20ICDE13.pdf)